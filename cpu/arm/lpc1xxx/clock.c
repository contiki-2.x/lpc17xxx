#include <sys/clock.h>
#include <sys/etimer.h>

#include "lpc17xx_systick.h"

#define CLK_CNT 1000

static volatile clock_time_t current_clock = 0;
static volatile unsigned long current_time = 0;
static unsigned long countdown = CLK_CNT;

void SysTick_Handler (void) {
  current_clock++;
  if(etimer_pending() && etimer_next_expiration_time() <= current_clock) {
    etimer_request_poll();
    /* printf("%d,%d\n", clock_time(),etimer_next_expiration_time  	()); */
  }

  if(--countdown == 0) {
	++current_time;
	countdown = CLK_CNT;
  }
}

void clock_init() {
  SysTick_Config(SystemCoreClock/CLK_CNT - 1); /* Generate interrupt each 1 ms   */
}

clock_time_t clock_time(void) {
  return current_clock;
}

unsigned long clock_seconds() {
  return current_time;
}

void clock_delay(unsigned int ticks) {
  unsigned long systickcnt = 0;

  systickcnt = current_clock;
  while ((current_clock - systickcnt) < ticks);
}

