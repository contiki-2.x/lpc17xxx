#include "debug_frmwrk.h"
#include "debug-uart.h"

void 
dbg_setup_uart() {
  debug_frmwrk_init();
}

unsigned int
dbg_send_bytes(const unsigned char *seq, unsigned int len) {
  const unsigned char* end = seq + len;
  while (seq < end) {
	UARTPutChar(DEBUG_UART_PORT, *seq++);
  }
  return len;
}

void
dbg_putchar(int ch) {
	UARTPutChar(DEBUG_UART_PORT, ch);
}
  
