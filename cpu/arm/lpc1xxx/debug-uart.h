#ifndef __DEBUG_UART_H__
#define __DEBUG_UART_H__

void
dbg_setup_uart();

unsigned int
dbg_send_bytes(const unsigned char *seq, unsigned int len);

void
dbg_putchar(int ch);

#endif /* __DEBUG_UART_H__ */
