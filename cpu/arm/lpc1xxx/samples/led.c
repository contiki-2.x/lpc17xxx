#include "lpc17xx_gpio.h"
#include "lpc17xx_systick.h"

#define LED_NUM     8
//const unsigned long const led_mask[] = { 1UL<<0, 1UL<<1, 1UL<<2, 1UL<<3, 1UL<<4, 1UL<<5, 1UL<<6, 1UL<<7 };
const unsigned char const led_mask[] = { 1<<0, 1<<1, 1<<2, 1<<3, 1<<4, 1<<5, 1<<6, 1<<7 };

volatile unsigned long SysTickCnt;

void SysTick_Handler (void) {
  SysTickCnt++;
}

void Delay (unsigned long tick) {
  unsigned long systickcnt;

  systickcnt = SysTickCnt;
  while ((SysTickCnt - systickcnt) < tick);
}

int main (void) {                       /* Main Program                       */
  int num = -1;
  int dir =  1;

  SystemInit();

  SysTick_Config(SystemCoreClock/1000 - 1); /* Generate interrupt each 1 ms   */

  // Looks like LCD_DIR needs to be pulled high
  GPIO_SetDir(0, 0x00200000, 1);
  GPIO_SetValue(0, 0x00200000);
  
  //*((uint32_t*)0x2009C040) = 0xff; //dir
  GPIO_SetDir(2, 0x000000FF, 1);           /* LEDs on PORT2 defined as Output 0-7   */
  GPIO_ClearValue(2, 0x000000FF);

  for (;;) {                            /* Loop forever                       */
    /* Calculate 'num': 0,1,...,LED_NUM-1,LED_NUM-1,...,1,0,0,...             */
    num += dir;
    if (num == LED_NUM) { dir = -1; num =  LED_NUM-1; }
    else if   (num < 0) { dir =  1; num =  0;         }

	//*((uint32_t*)0x2009C058) = led_mask[num];
	GPIO_SetValue(2, led_mask[num]);
	Delay(500);                         /* Delay 500ms                        */
	
	//*((uint32_t*)0x2009C05c) = led_mask[num];
	GPIO_ClearValue(2, led_mask[num]);
	Delay(500);                         /* Delay 500ms                        */
  }
}

