/**
 * \file
 *         A very simple Contiki application showing how Contiki programs look
 */

#include "contiki.h"

#include <stdio.h> /* For printf() */

#include "lpc17xx_gpio.h"
#include "lpc17xx_pinsel.h"
#include "lpc17xx_pwm.h"
#include "lpc17xx_clkpwr.h"

#define LED_NUM     6 // 2-7
#define PWM_CHANNEL 1

void init_led() {
  // Looks like LCD_DIR needs to be pulled high
  GPIO_SetDir(0, 0x00200000, 1);
  GPIO_SetValue(0, 0x00200000);
  
  // P2[0] is PWM1[1]
  // using PWM1[2], P2[1]
  //*((uint32_t*)0x2009C040) = 0xff; //dir
  GPIO_SetDir(2, 0x000000FC, 1);           /* LEDs on PORT2 defined as Output 0-7   */
  GPIO_ClearValue(2, 0x000000FF);
}

void init_pwm() {
  // PWM
  // TC incremented PR+1 cycles
  // PR prescale reg, when PC is equal to this, next clock increments TC, clears PC
  // PC incremented every cycle
  // 
  // in counter mode, used to count external events on PCAP pin
  // CTCR in counter mode, signal PCAP1_0 & rising edges
  /*
  PWM_TIMERCFG_Type timercfg;
  timercfg.PrescaleOption = PWM_TIMER_PRESCALE_TICKVAL;
  timercfg.PrescaleValue = 1;
  PWM_ConfigStructInit(PWM_MODE_TIMER, &timercfg);
  */
  // step 1 Enable PCM in PCOMP
  LPC_SC->PCONP |= CLKPWR_PCONP_PCPWM1 & CLKPWR_PCONP_BITMASK;
  // setp 2, set peripheral clock, PCLKSEL0
  /* Clear two bit at bit position */
  LPC_SC->PCLKSEL0 &= (~(CLKPWR_PCLKSEL_BITMASK(CLKPWR_PCLKSEL_PWM1)));
  /* Set two selected bit */
  LPC_SC->PCLKSEL0 |= (CLKPWR_PCLKSEL_SET(CLKPWR_PCLKSEL_PWM1, CLKPWR_PCLKSEL_CCLK_DIV_4));
  //PWM_Init(LPC_PWM1, PWM_MODE_TIMER, &timercfg);

  // Step 3, select the pin 2 as PWM
  PINSEL_CFG_Type pincfg;
  pincfg.Portnum = PINSEL_PORT_2;
  pincfg.Pinnum = PINSEL_PIN_0;
  pincfg.Funcnum = PINSEL_FUNC_1;
  pincfg.Pinmode = PINSEL_PINMODE_PULLDOWN;
  pincfg.OpenDrain = PINSEL_PINMODE_OPENDRAIN;
  PINSEL_ConfigPin(&pincfg);
  pincfg.Pinnum = PINSEL_PIN_1;
  PINSEL_ConfigPin(&pincfg);

  // Clear all interrupts pending
  LPC_PWM1->IR = 0xFF & PWM_IR_BITMASK;
  LPC_PWM1->TCR = 0x00;  // later
  LPC_PWM1->CTCR = 0x00; // timer mode, tc incremented when prescale ctr == prescale reg (PR)
  LPC_PWM1->MCR = 0x00;
  LPC_PWM1->CCR = 0x00;
  LPC_PWM1->PCR = 0x00;
  LPC_PWM1->LER = 0x00;
  LPC_PWM1->PR = 0x00;

  // Step 4, when TC matches MCR0 - dont interrupt, reset counter, dont stop timer 
  /*
  PWM_MATCHCFG_Type match;
  match.IntOnMatch = DISABLE;
  match.ResetOnMatch = ENABLE;
  match.StopOnMatch = DISABLE;
  match.MatchChannel = PWM_CHANNEL;
  PWM_ConfigMatch(LPC_PWM1, &match);
  */
  LPC_PWM1->MCR &= (~PWM_MCR_INT_ON_MATCH(0))	& PWM_MCR_BITMASK;
  LPC_PWM1->MCR |= PWM_MCR_RESET_ON_MATCH(0);
  LPC_PWM1->MCR &= (~PWM_MCR_STOP_ON_MATCH(0)) & PWM_MCR_BITMASK;

  //PWM_ConfigCapture CCR, used for counter only
  
  // PCR->PWMSEL single edge mode and channel selection
  //PWM_ChannelConfig(LPC_PWM1, PWM_CHANNEL, PWM_CHANNEL_SINGLE_EDGE);
  //PWM_ChannelCmd
  LPC_PWM1->PCR &= (~PWM_PCR_PWMSELn(PWM_CHANNEL)) & PWM_PCR_BITMASK; 
  LPC_PWM1->PCR |= PWM_PCR_PWMENAn(PWM_CHANNEL);

  // writes latch reg to enable matching on channel, MR, LER & reset
  //PWM_MatchUpdate(LPC_PWM1, PWM_CHANNEL, SysTickCnt, PWM_MATCH_UPDATE_NOW);
  LPC_PWM1->MR0 = 1000;
  LPC_PWM1->MR1 = 100;
  LPC_PWM1->LER |= PWM_LER_EN_MATCHn_LATCH(PWM_CHANNEL);

  // counter enable, PWM->TCR
  //PWM_CounterCmd(LPC_PWM1, ENABLE);
  // start, PWM->TCR must set match before this
  //PWM_Cmd(LPC_PWM1, ENABLE);
  //PWM_ResetCounter(LPC_PWM1);
  LPC_PWM1->TCR	|=  PWM_TCR_PWM_ENABLE | PWM_TCR_COUNTER_ENABLE | PWM_TCR_COUNTER_RESET;
  LPC_PWM1->TCR &= (~PWM_TCR_COUNTER_RESET) & PWM_TCR_BITMASK;

}

/*---------------------------------------------------------------------------*/
PROCESS(blink_process, "Blink process");
PROCESS(pwm_process, "PWM proces");
AUTOSTART_PROCESSES(&blink_process, &pwm_process);
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(blink_process, ev, data)
{
  PROCESS_BEGIN();

  init_led();

  printf("Start blinker\n");
  static int num = -1;
  static int dir =  1;

  static const unsigned char const led_mask[] = { /* 1<<0, 1<<1,*/ 1<<2, 1<<3, 1<<4, 1<<5, 1<<6, 1<<7 };

  static struct etimer timer;
  etimer_set(&timer, CLOCK_CONF_SECOND/10);

  for(;;) {
	PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
/* Calculate 'num': 0,1,...,LED_NUM-1,LED_NUM-1,...,1,0,0,...             */
    num += dir;
    if (num == LED_NUM) { dir = -1; num =  LED_NUM-1; }
    else if   (num < 0) { dir =  1; num =  0;         }

	//*((uint32_t*)0x2009C058) = led_mask[num];
	GPIO_SetValue(2, led_mask[num]);

	etimer_reset(&timer);
	PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);
	
	//*((uint32_t*)0x2009C05c) = led_mask[num];
	GPIO_ClearValue(2, led_mask[num]);
	etimer_reset(&timer);
  }
  
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(pwm_process, ev, data)
{
  PROCESS_BEGIN();

  init_pwm();

  printf("Start pwm \n");

  static const int reduce = 100;
  static int power = 1000;

  static struct etimer timer;
  etimer_set(&timer, CLOCK_CONF_SECOND/5);

  while(1) {
	PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_TIMER);	
	// update match
	power = power == 0 ? 1000 : power - reduce;
	PWM_MatchUpdate(LPC_PWM1, PWM_CHANNEL, power, PWM_MATCH_UPDATE_NEXT_RST);
	etimer_reset(&timer);
  }

  PROCESS_END();
}
