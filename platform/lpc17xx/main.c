#include <stdio.h>

#include <sys/clock.h>
#include <sys/etimer.h>
#include <sys/autostart.h>

#include "lpc17xx_clkpwr.h"
#include "debug-uart.h"

int main (void) {                       /* Main Program                       */
  SystemInit();
  clock_init();
  dbg_setup_uart();

  printf("Init done\n");
  //rtimer_init();
  process_init();
  process_start(&etimer_process, NULL);

  printf("starting Processes\n");
  autostart_start(autostart_processes);
  printf("Processes running\n");

  for (;;) {                            /* Loop forever                       */

	do {
	} while(process_run() > 0);
    
  }
  
  printf("Error, Exiting\n");
}

